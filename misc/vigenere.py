"""
Script per il Cifrario di Vigenere

DO NOT USE FOR ANY SECURITY PURPOSE!
"""

baseAlpha = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

def shiftAlpha():
    alpha = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    yield [a for a in alpha]
    for i in range(26):
        alpha.append(alpha.pop(0))
        yield [a for a in alpha]

table = {c[0] : c for c in shiftAlpha()}
countertable = {c[0] : {c[i] : baseAlpha[i] for i in range(len(c))} for c in shiftAlpha()}

def vigenere(plaintext, key):
    return [table[key[i % len(key)]][baseAlpha.index(plaintext[i])] for i in range(len(plaintext))]

def unvigenere(ciphertext, key):
    return [countertable[key[i % len(key)]][ciphertext[i]] for i in range(len(ciphertext))]

def encode():
    plaintext = input("Inserisci il messaggio (solo lettere): ").upper().replace(" ", "")
    key = input("Inserisci la chiave (solo lettere): ").upper().replace(" ", "")
    ciphertext = vigenere(plaintext, key)
    print("".join(ciphertext))

def decode():
    ciphertext = input("Inserisci il messaggio cifrato (solo lettere): ").upper().replace(" ", "")
    key = input("Inserisci la chiave (solo lettere): ").upper().replace(" ", "")
    plaintext = unvigenere(ciphertext, key)
    print("".join(plaintext))

if __name__ == '__main__':
    print("Cifrario di Vigenere\n")
    command = input("Premi C per cifrare, D per decifrare, E per uscire: ").upper()
    actions = {
        "C" : encode,
        "D" : decode
    }
    while command != "E":
        if command in actions:
            actions[command]()
        else:
            print("Comando non valido")

        command = input("Premi C per cifrare, D per decifrare, E per uscire: ").upper()

    print("Smell ya")
