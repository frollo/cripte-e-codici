# Cripte e Codici
### Una guida all'uso della crittografia per Game Master

Cripte e Codici punta a presentare nuove (cioè, vecchie ma poco note) opzioni ai GM interessati a mettere i propri giocatori davanti a sfide di crittografia.

Si tratta di un compendio non ufficiale e slegato da qualsiasi sistema (anche se la veste grafica ricorda molto i supplementi di D&D 5, ma solo per ragioni estetiche).

## Ringraziamenti

Il template per la grafica viene da [D&D 5e LaTeX Template](https://github.com/evanbergeron/DND-5e-LaTeX-Template)

Grazie a IntegraleDoppio per la revisione bozze e Francesco e Gian per i cifrari da usare come esempio.

## Download

La versione più aggiornata di questo compendio può essere scaricata [qua](https://gitlab.com/frollo/cripte-e-codici/raw/master/cripte-e-codici.pdf)

## Errori, lamentele e proposte

Nel caso trovaste un qualsiasi errore (di lingua, di contenuto o altro) aveste delle rimostranze o delle proposte, siete invitati a presentarmi la vostra opinione in uno di questi tre modi:
  1. Inviando un'email a incoming+frollo/cripte-e-codici AT gitlab.com (questo aprirà un issue esattamente come al punto 2, ma non vi serve un account GitLab per farlo)
  2. Aprendo un issue [qui](https://gitlab.com/frollo/cripte-e-codici/issues)
  3. Facendo una pull request con le vostre modifiche

Il metodo preferito per proposte e correzioni è, ovviamente, la pull request.

## Build

Per chi volesse contribuire (o semplicemente compilarsi in locale il documento), per compilare è necessario installare o includere nella cartella [D&D 5e LaTeX Template](https://github.com/evanbergeron/DND-5e-LaTeX-Template). Il PDF in download viene generato con 3 passate consecutive di `pdflatex`.

## Licenza

Cripte e Codici è rilasciato con licenza [Licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale.](https://creativecommons.org/licenses/by-sa/4.0/)
